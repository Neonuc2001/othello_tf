#import tensorflow as tf
import numpy as np
import datetime
import time
import othello
import chainer
from chainer import serializers, Sequential, Chain
import chainer.links as L
import chainer.functions as F




def forward(x, y, model):
    t = model.predict(x)
    loss = F.mean_squared_error(y, t)
    return loss

test=np.full((4,4),-1)
test[1][1]=0
test[1][2]=0
print(type(test))
print(test)


if -0==0:
    test=test*-1
    print(type(test))
    print(test)

test=test*-1
print(type(test))
print(test)
k=othello.othelloReverse(test)
print(type(k))
print(k)
print(type(k))
print(k)

"""
ここでは学習データの作成と学習を連続して行う。
学習して得られたモデルをもとに新しく学習データを作り、それを再び学習するという感じ。
当然あまりに高い水準の学習結果を得られる前に打ち切る必要がある（特に事実上の外れ値が存在しない以上過学習は起きて当然）

"""
"""
#テスト用
result=0
board=othello.othelloInit()
for i in range(0,2):
    print(i)
while othello.othelloEndCheck(board)==1 or othello.othelloEndCheck(othello.othelloReverse(board))==1:
    axis=[0,0]
    othello.othelloShow(board)
    axis[0]=int(input("横の座標を入力してください。\n"))
    axis[1]=int(input("縦座標を入力してください\n"))
    temp=othello.othelloMove(board,axis)
    if type(temp) ==np.ndarray:
        board=temp
    board=othello.othelloReverse(board)
    k=othello.othelloMove(board,othello.othelloRandMove(board))
    if type(k)==np.ndarray:
        board=k
    board=othello.othelloReverse(board)
othello.othelloShow(board)
for k in range(0,8):
    for i in range(0,8):
        result=result+board[k][i]
print(result)
"""
#本番

#import tensorflow as tf
dataset=[]
dataset_m=[]
for i in range(0,20):
   

    board=othello.othelloInit()
    b=[]
    b_m=[]
    w=[]
    w_m=[]
    Flag=0
    #othello.othelloShow(board)
    while othello.othelloEndCheck(board)==1 or othello.othelloEndCheck(othello.othelloReverse(board))==1:
        #time.sleep(0.2)
        
        k=0
      #  print("先行")
        t=othello.othelloRandMove(board)
        #print(t)
        if not type(t) is int:
            
            k=othello.othelloMove(board,t)

        else:
         #   print("打ち手なし")
            #time.sleep(1)
            Flag=1
        if type(k)==np.ndarray:
            b.append(board)
            b_m.append(t)
            board=k
      #  othello.othelloShow(board)
        board=othello.othelloReverse(board)
        k=0
     #   print("\n後攻")
        
        t=othello.othelloRandMove(board)
        #print(t)
        if not type(t) is int:
            
            k=othello.othelloMove(board,t)

        else:
       #     print("打ち手なし")
            #time.sleep(1)
            Flag=1
        if type(k)==np.ndarray:
            w.append(board)
            w_m.append(t)
            board=k
        
        board=othello.othelloReverse(board)
       # othello.othelloShow(board)
    print("one game finished")
    print("i:"+str(i))
    
    if othello.othelloWinCheck(board)>=0:
        print("先行勝ち")
        #print(b)
        
        dataset=dataset+b
        dataset_m=dataset_m+b_m
    else:
        print("後攻勝ち")
        #print(w)
        
        dataset=dataset+w
        dataset_m=dataset_m+w_m
    #time.sleep(1)

dataset=np.array(dataset,dtype=type(dataset))
dataset_m=np.array(dataset_m,dtype=type(dataset_m))
print(type(dataset))
print(dataset)

time.sleep(1)

time.sleep(1)

"""
model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(8, 8)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.1),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.1),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.1),
  tf.keras.layers.Dense(2)
])

model.compile(optimizer='adam', 
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
while 1==1:
    model.fit(x, y, epochs=5)
    test_loss, test_acc = model.evaluate(x,  y, verbose=2)
    print('\nTest accuracy:', test_acc)
    input()
"""






