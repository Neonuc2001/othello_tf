import numpy as np
import random
import copy
board_size=8
board_value=[[120,-20,20,5],[-20,-40,-5,-5],[20,-5,15,3],[5,-5,3,3]]
#左下を0,0とする
def othelloInit():
    board = np.zeros((8,8))
    board[4-1][4-1]=1
    board[5-1][5-1]=1
    board[4-1][5-1]=-1
    board[5-1][4-1]=-1
    board=board.astype(np.int32)
    return board

def othelloMove(board,move):
    if type(move)==int:
        return -1

    if move[0]<board_size or move[0]>=0:
        tempBoard=othelloMovable(board,move)
    if type(tempBoard) ==np.ndarray:
        return tempBoard
    else:   
        return 0

def othelloBlank(board,move):#空いてるなら0、空いてないなら1
    k=(board[move[0]][move[1]])*(board[move[0]][move[1]])

    return k
def othelloMovable(Board,Move):#moveにおけるかどうかを判定、おけるなら置いた後の盤面情報、おけないなら０を返す
    board=Board.copy()
    move=copy.deepcopy(Move)
    old_board=board.copy()
    if move[0]<0:
        move[0]=0
    elif move[0]>=8:
        move[0]=7
    if move[1]<0:
        move[1]=0
    elif move[1]>=8:
        move[1]=7
    if othelloBlank(board,move)==1:
        return 0
    for i in range(1,9):
        
        #print("ColorCheck:"+str(ColorCheck(board,move,i))+" i:"+str(i))
        if ColorCheck(board,move,i)==-1:
            if i==1:#上
                #print("i:"+str(i))
                check=1
                for k in range(move[1],8):#自身を含めたマスのチェック
                    
                    if k==move[1]:#自分は調べ済みなのでパス
                        pass
                    else:
                        #print([move[0],k])
                        temp=ColorCheck(board,[move[0],k],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                        
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            
                            check=check*-1
                        else:#おいても反転しないので除外
                            
                            break
                    if check<0:#負の値の場合はおける
                        
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[move[0]][c+move[1]]=1
                        break
            elif i==2:#右上
                #print("i:"+str(i))
                temp_move=max(move)
                check=1
                for k in range(temp_move,8):#自身を含めたマスのチェック
                    
                    if k==temp_move:#自分は調べ済みなのでパス
                        pass
                    else:
                        temp=ColorCheck(board,[move[0]+k-temp_move,move[1]+k-temp_move],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            check=check*-1
                        else:#おいても反転しないので除外
                            
                            break
                    if check<0:#負の値の場合はおける
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[c+move[0]][c+move[1]]=1
                        break
            elif i==3:#右
                #print("i:"+str(i))
                check=1
                for k in range(move[0],8):#自身を含めたマスのチェック
                    
                    if k==move[0]:#自分は調べ済みなのでパス
                        pass
                    else:
                        temp=ColorCheck(board,[k,move[1]],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                          
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            
                            check=check*-1
                        else:#おいても反転しないので除外
                            
                            break
                    if check<0:#負の値の場合はおける
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[move[0]+c][move[1]]=1
                        break
            elif i==4:#右下
                #print("i:"+str(i))
                temp_move=max(move[0],7-move[1])
                check=1
                for k in range(temp_move,8):#自身を含めたマスのチェック
                    
                    if k==temp_move:#自分は調べ済みなのでパス
                        pass
                    else:
                        temp=ColorCheck(board,[move[0]+k-temp_move,move[1]-k+temp_move],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            check=check*-1
                        else:#おいても反転しないので除外
                            
                            break
                    if check<0:#負の値の場合はおける
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[c+move[0]][-c+move[1]]=1
                        break
            elif i==5:#下
                #print("i:"+str(i))
                check=1
                for k in range(move[1],0,-1):#自身を含めたマスのチェック
                    
                    if k==move[1]:#自分は調べ済みなのでパス
                        pass
                    else:
                        temp=ColorCheck(board,[move[0],k],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            check=check*-1
                        else:#おいても反転しないので除外
                            
                            break
                    if check<0:#負の値の場合はおける
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[move[0]][-c+move[1]]=1
                        break
            elif i==6:#左下
                #print("i:"+str(i))
                temp_move=min(move)
                check=1
                for k in range(temp_move,0,-1):#自身を含めたマスのチェック
                    
                    if k==temp_move:#自分は調べ済みなのでパス
                        pass
                    else:
                        temp=ColorCheck(board,[move[0]+k-temp_move,move[1]+k-temp_move],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            check=check*-1
                        else:#おいても反転しないので除外
                            
                            break
                    if check<0:#負の値の場合はおける
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[-c+move[0]][-c+move[1]]=1
                        break
            elif i==7:#左
                #print("i:"+str(i))
                check=1
                for k in range(move[0],0,-1):#自身を含めたマスのチェック
                    
                    if k==move[0]:#自分は調べ済みなのでパス
                        pass
                    else:
                        temp=ColorCheck(board,[k,move[1]],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            check=check*-1
                        else:#おいても反転しないので除外
                           
                            break
                            
                    if check<0:#負の値の場合はおける
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[move[0]-c][move[1]]=1
                        break
            elif i==8:#左上
                #print("i:"+str(i))
                temp_move=max(7-move[0],move[1])
                check=1
                for k in range(temp_move,8):#自身を含めたマスのチェック
                    
                    if k==temp_move:#自分は調べ済みなのでパス
                        pass
                    else:
                        
                        temp=ColorCheck(board,[move[0]-k+temp_move,move[1]+k-temp_move],i)#マスチェック
                        #print("Check:"+str(check))
                        if temp==-1:#相手ならカウントを増やす
                            check=check+1
                        elif temp==1 and check>0:#自分なら反転、一度も相手のマスが出なかった時を除外
                            check=check*-1
                        else:#おいても反転しないので除外
                            
                            break
                    if check<0:#負の値の場合はおける
                        for c in range(0,(check*-1)+1):#カウントの値を参照して変更するべきマスを確定させる。
                            board[-c+move[0]][c+move[1]]=1
                        break
    if np.allclose(board,old_board):
        #print("置けないです")
        return 0
    return board

def othelloEndCheck(board):#もうおける場所がないかを調べる。おけるなら1、置けないなら０を返す。
    for i in range(0,8):
        for n in range(0,8):
            #print([i,n])
            if type(othelloMovable(board,[i,n]))==np.ndarray:
                #print("Nope")
                return 1
    return 0

def ColorCheck(board,axis,way):#時計回りで上が１スタートし、8まで予約。0は全方位。盤外なら-2,相手なら-1、空白なら0、自身なら１を返す。
    if way==1:
        if axis[1]>=7:
            return -2
        else:
            #print("ColorCheck")
            #print(board[axis[0]][axis[1]+1])
            return board[axis[0]][axis[1]+1]
    elif way==2:
        if axis[0]>=7 or axis[1]>=7:
            return -2
        else:
            return board[axis[0]+1][axis[1]+1]
    elif way==3:
        if axis[0]>=7:
            return -2
        else:
            return board[axis[0]+1][axis[1]]
    elif way==4:
        if axis[0]>=7 or axis[1]<=0:
            return -2
        else:
            return board[axis[0]+1][axis[1]-1]       
    elif way==5:
        if axis[1]<=0:
            return -2
        else:
            return board[axis[0]][axis[1]-1]
    elif way==6:
        if axis[1]<=0 or axis[0]<=0:
            return -2
        else:
            return board[axis[0]-1][axis[1]-1]
    elif way==7:
        if axis[0]<=0:
            return -2
        else:
            return board[axis[0]-1][axis[1]]
    elif way==8:
        if axis[0]<=0 or axis[1]>=7:
            return -2
        else:
            return board[axis[0]-1][axis[1]+1]


    return 0

def othelloReverse(Board):
    board=Board.copy()
    #print("reversing")
    board=board*-1
    return board

def othelloShow(board):
    for i in reversed(range(0,8)):#y
        #print("i:"+str(i))
        for k in range(0,8):#x
            if board[k][i]==-1:
                print("○",end=" ")
            elif board[k][i]==1:
                print("●",end=" ")
            elif board[k][i]==0:
                print("■",end=" ")
        print("")
        print("")
    #print(board[7])
    return 0

def othelloMoveChoice(board):
    choice_list=[]
    for i in range(0,8):
        for n in range(0,8):
            if type(othelloMovable(board,[i,n]))==np.ndarray:
                #print("[i,n];",end="")
                #print([i,n])
                choice_list.append([i,n])
    return choice_list

def othelloRandMove(Board):
    board=Board
    current_value=-300
    current_choice=[]
    choice_list=othelloMoveChoice(board)
    try:
        for i in choice_list:
            if current_value < board_value[min(i[0],7-i[0])][min(i[1],7-i[1])] and random.randint(1,2)==1:
                current_value=board_value[min(i[0],7-i[0])][min(i[1],7-i[1])]
                current_choice=i
        if current_choice==[]:
            current_choice=random.choice(choice_list)
        return current_choice
    except IndexError:
        #print("choice_list:")
        #print(choice_list)
        #print("")
        return -1

def othelloWinCheck(board):
    result=0
    for k in range(0,8):
        for i in range(0,8):
            result=result+board[k][i]
    return result